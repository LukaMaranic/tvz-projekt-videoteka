<?php


namespace App\Controller;


use App\Repository\MovieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/home", name="homepage_index", methods={"GET"})
     */
    public function index(MovieRepository $movieRepository): Response
    {

        $package = new Package(new EmptyVersionStrategy());

        return $this->render('homepage/homepage.html.twig');
    }
}